<?php

namespace Drupal\visually_impaired_module\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Defines the theme negotiator.
 */
class ThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Route Admin Context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $routerAdminContext;

  /**
   * ThemeNegotiator constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *
   *   The Config Factory.
   * @param \Drupal\Core\Routing\AdminContext $router_admin_context
   *
   *   The Router Admin Context.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AdminContext $router_admin_context) {
    $this->configFactory = $config_factory;
    $this->routerAdminContext = $router_admin_context;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $config = $this->configFactory->get('visually_impaired_module.visually_impaired_module.settings');

    $is_admin = $this->routerAdminContext->isAdminRoute($route_match->getRouteObject());

    if ((isset($_COOKIE['visually_impaired'])) && ($_COOKIE['visually_impaired'] == 'on') && ($is_admin == FALSE)) {
      return $config->get('visually_impaired_theme');
    }
  }

}
