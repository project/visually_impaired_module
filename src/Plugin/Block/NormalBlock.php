<?php

namespace Drupal\visually_impaired_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Normal' Block.
 *
 * @Block(
 *   id = "normal_block",
 *   admin_label = @Translation("Normal block"),
 * )
 */
class NormalBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = $this->formBuilder->getForm('Drupal\visually_impaired_module\Form\VINormalForm');

    $classes = [];
    $this->configuration['block_style'] ?
      $classes[] = 'vi-normal-block-image' :
      $classes[] = 'vi-normal-block-text';

    $form['visually-impaired-normal-block']['#attributes']['class'] = $classes;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'block_style' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $active = [
      0 => $this->t('Text'),
      1 => $this->t('Image'),
    ];

    $form['block_style'] = [
      '#type' => 'radios',
      '#title' => $this->t('Block style'),
      '#required' => TRUE,
      '#options' => $active,
      '#default_value' => $this->configuration['block_style'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['block_style'] = $form_state->getValue('block_style');
  }

}
