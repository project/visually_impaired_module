# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

Module provides version for visually impaired people according to Russian
legislation

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/visually_impaired_module>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/visually_impaired_module>


## REQUIREMENTS

 * This module requires no modules outside of Drupal core
 * Visually Impaired Theme (optional) - 
   <https://drupal.org/project/visually_impaired_theme>


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > User interface > Visually 
       Impaired Support and set "visually impaired" theme
    3. Save configuration.
    4. Navigate to Administration > Structure > Block layout and add block 
       "visually impaired version block" to main theme
    5. To "visually impaired" theme add block "normal version block"


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
